(function preferences_prompt() {
  var $submit = document.getElementById("preferences_prompt_submit"),
      $modal = document.getElementById("__tealiumGDPRcpPrefs"),
      $closeBtn = $modal.getElementsByClassName("close_btn_thick")[0],
      $body = $modal.getElementsByClassName("consent_preferences")[0],
      $dimmer = $modal.getElementsByClassName("pp_dimmer")[0],
      reg_match = /\d+$/,
      i,
      elem;

  /*** Save consent preferences ***/
  var savePrefs = function () {
    var inputs = $body.getElementsByClassName("toggle"),
    cats = {};
    for (var i = 0; i < inputs.length; i++) {
      var obj = inputs[i];
      cats[obj.id.match(reg_match)[0]] = obj.checked ? 1 : 0;
    }
    closePrompt();
    utag.gdpr.setPreferencesValues(cats);
    console.log("Consent preferences changed: ", cats);
  };

  /*** Close prompt ***/
  var closePrompt = function () {
    $body.style.right = "-320px";
    $dimmer.style.opacity = 0;
    document.body.style.overflow = "visible";
    setTimeout(function () {
      $dimmer.style.display = "none";
      $modal.style.display = "none";
    }, 500)
  };

  /*** Stop event propagation ***/
  var stopPropagation = function (e) {
    if (e.stopPropagation) {
      e.stopPropagation();
    }
  };
    
  /*** Init consent state ***/
  var consentState = utag.gdpr.getConsentState();
  if (typeof consentState === "number") {
    var _state = false;
    if (consentState === 1 || consentState === -1){
      _state = consentState === 1;
    } else {
      _state = !!utag.gdpr.preferences_prompt.defaultState;
    }
    for (i = 0; i < utag.gdpr.getCategories().length; i++) {
      elem = document.getElementById("toggle_cat" + (i + 1));
      if (elem) {
        elem.checked = _state;
      }
    }
  } else {
    for (i = 0; i < consentState.length; i++) {
      elem = document.getElementById("toggle_cat" + (i + 1));
      if (elem) {
        elem.checked = consentState[i].ct === "1" ? true : false;
      }
    }
  }
    
  /*** Add event listeners ***/
  if (document.addEventListener) {
    $closeBtn.addEventListener("click", closePrompt, false);
    $body.addEventListener("click", stopPropagation);
    $dimmer.addEventListener("click", closePrompt, false);
    $submit.addEventListener("click", savePrefs, false);
  } else if (document.attachEvent) {
    $closeBtn.attachEvent("click", closePrompt);
    $body.attachEvent("click", stopPropagation);
    document.attachEvent("click", closePrompt);
    $submit.attachEvent("click", savePrefs, false);
  } else {
    $closeBtn.onclick = closePrompt;
    $submit.onclick = savePrefs;
  }

  /*** Remove scroll on body ***/
  document.body.style.overflow = "hidden";

  /*** Slide the menu ***/
  setTimeout(function () {
    $body.style.right = 0;
    $dimmer.style.opacity = 1;
    $dimmer.style.display = "block";
    $modal.style.display = "block";
  }, 0)

}());