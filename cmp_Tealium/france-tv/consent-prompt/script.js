(function consent_prompt(){
  var $modal = document.getElementById("__tealiumGDPRecModal"),
      $closeBtn = $modal.getElementsByClassName("close_btn_thick")[0],
      $consentPrefsButton = $modal.getElementsByClassName("consent_prefs_button")[0];

  /*** Save consent, close prompt and show consent preferences ***/
  var showPrefs = function (e) {
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    utag.gdpr.showConsentPreferences();
    closePrompt(e);
  };

  /*** Close prompt and save consent ***/
  var closePrompt = function(e) {
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    $modal.style.display = "none";
    if (document.addEventListener) {
      document.removeEventListener("click", closePrompt, false);
    } else if (document.detachEvent) {
      document.detachEvent("click", closePrompt);
    }
    utag.gdpr.setConsentValue(1);
    
    /*** needs to be updated depending on the categories used in the consent preferences ***/
    var allcats = {1: 1, 3: 1, 6: 1, 8: 1};
    utag.gdpr.setPreferencesValues(allcats);
    console.log("Consent given: ", allcats);
  };

  /*** Add event listeners ***/
  if (document.addEventListener) {
    $closeBtn.addEventListener("click", closePrompt, false);
    $consentPrefsButton.addEventListener("click", showPrefs, false);
    document.addEventListener("click", closePrompt, false);
  } else if (document.attachEvent) {
    $closeBtn.attachEvent("click", closePrompt);
    $consentPrefsButton.attachEvent("click", showPrefs, false);
    document.attachEvent("click", closePrompt);
  } else {
    $closeBtn.onclick = closePrompt;
    $consentPrefsButton.onClick = showPrefs;
  }
}());