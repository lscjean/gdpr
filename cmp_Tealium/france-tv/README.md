Chaque Tealium Profile détient son propre Fichier 'Consent Manager'. 
Pour accéder aux différents éléments de ceux-ci, veuillez accéder aux fichiers correspondants:
- le consent prompt (ou bandeau CNIL) se retrouve avec le label "prompt".
- le consent preference dialogue (préférences) se retrouve avec le label "prefs"

Dans chaque fichier HTML se trouve 3 éléments: 
- le CSS (style) à l'intérieur de la balise <style></style>
- le HTML (forme) à l'intérieur de la balise <div></div>
- le Javascript (fonctions) à l'intérieur de la balise <script></script>
Vous n'avez pas besoin de copier les balise Style, Div & Script dans Tealium

IMPORTANT : les codes ne sont compatibles qu'avec la technologies de Tag Management "Tealium IQ" pour la section consent manager. 
Veuillez vous référer à la documentation fournie par Mediascale Benelux pour l'intégration des designs sur les différents profiles. 

Si vous avez la moindre question, merci de contacter votre account manager benelux@mediascale.eu <beneluxmediascaleeu@serviceplan.com>
En vous souhaitant une bonne journée

Jean Lissac
Integration & MarTech Manager. 
+32 (0)498 13 98 91
@mediascaleBenelux
</ciao>