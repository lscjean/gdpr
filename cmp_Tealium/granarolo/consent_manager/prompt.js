
/***********************DO NOT MODIFY BELOW THIS LINE***********************/
(function consent_prompt(){
    var $el = document.getElementById("consent_prompt_submit"),
        $bg_el = document.getElementById("bg_layer"),    
        $cookie_page = document.getElementById("cookie_page"),
        $closeBtn = document.getElementsByClassName("close_btn_thick")[0],
        $privacy_pref_optin = document.getElementById("privacy_pref_optin"),
        $privacy_pref_optout = document.getElementById("privacy_pref_optout");
        $privacy_prompt = document.getElementsByClassName("privacy_prompt")[0];
        $dialogue_box = document.getElementById("dialogue_box");
        $boolConsent = 0; 
        
    var consentState = utag.gdpr.getConsentState();
    if (typeof consentState === "number") {
      if (consentState === 1) {
        $privacy_pref_optin.checked = true;
      } else if (consentState === -1){
        $privacy_pref_optout.checked = true;
      }
    } else {
      $privacy_pref_optin.checked = true;
    }
    
    var callBack = function() {
        if ($boolConsent == 0){
            console.log('consent given!');
            utag.gdpr.setConsentValue(1);    
            setTimeout(function(){if (window.utag && window.utag.udoname || window.utag_data) {utag.view(window[window.utag && window.utag.udoname || "utag_data"]);}},0);
            $boolConsent = 1;
            closePrompt();
        }
    };
    var closePrompt = function(){
      var $privacy_prompt = document.getElementsByClassName("privacy_prompt")[0];
      var $layer = document.getElementById("bg_layer");
      $privacy_prompt.style.display = "none";
      $layer.style.display = "none";
    };
    var optoutClosePrompt = function(){
      var $privacy_prompt = document.getElementsByClassName("privacy_prompt")[0];
      var $layer = document.getElementById("bg_layer");
      utag.gdpr.setConsentValue(0);
      $privacy_prompt.style.display = "none";
      $layer.style.display = "none";
    };
    
    var showPreferences = function() {
    
      callBack();
      utag.gdpr.showConsentPreferences();
      
    };
    
    
    if (document.addEventListener) {
      $el.addEventListener("click", callBack, false);
      $closeBtn.addEventListener("click", optoutClosePrompt, false);
      $bg_el.addEventListener("click", callBack, false);
      $cookie_page.addEventListener("click", callBack, false);
      $dialogue_box.addEventListener("click", showPreferences, false);
      window.addEventListener('scroll',callBack, true);
    } else if (document.attachEvent) {
      $el.attachEvent("click", callBack);
      $cookie_page.attachEvent("click", callBack);
      $bg_el.attachEvent("click", callBack);
      $closeBtn.attachEvent("click", optoutClosePrompt);
      $dialogue_box.attachEvent("click", showPreferences);
      
    } else {
      $el.onclick = callBack;
      $bg_el.onclick = callBack;
      $cookie_page.onclick = callBack;
      $closeBtn.onclick = optoutClosePrompt;
      $dialogue_box.onclick = showPreferences;
    }
  }());