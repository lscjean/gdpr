


/***********************DO NOT MODIFY BELOW THIS LINE***********************/
(function preferences_prompt() {
    var $el = document.getElementById("consent_dialogue_submit"),
        $modal = document.getElementById("__tealiumGDPRcpPrefs"),
        $closeBtn = $modal.getElementsByClassName("close_btn_thick")[0],
        $body = document.getElementsByClassName("consent_dialogue")[0],
        
        reg_match = /\d+$/,
        i;
      var $privacy_footer = document.getElementById("__tealiumGDPRecModal");
      $privacy_footer.style.display = "none";
          document.getElementById("toggle_cat3").checked = true;
      document.getElementById("toggle_cat6").checked = true;
    
    var callBack = function () {
      
      var inputs = $body.getElementsByClassName("toggle"),
          cats = {};
      
      for (var i = 0; i < inputs.length; i++) {
        var obj = inputs[i];
        cats[obj.id.match(reg_match)[0]] = obj.checked ? 1 : 0;
      }
      closePrompt();
      
      utag.gdpr.setPreferencesValues(cats);
    };
    var closePrompt = function () {
      var $privacy_prompt = document.getElementById("__tealiumGDPRcpPrefs");
      $privacy_prompt.style.display = "none";
      
    };
    
    
    var consentState = utag.gdpr.getConsentState();
    if (typeof consentState === "number") {
      for (i = 0; i < utag.gdpr.getCategories().length; i++) {
        document.getElementById("toggle_cat" + (i + 1)).checked = (consentState === 1);
      }
    } else {
      for (i = 0; i < consentState.length; i++) {
        if (consentState[i].ct !== "1") {
          continue;
        }
        document.getElementById("toggle_cat" + (i + 1)).checked = true;
      }
    }
    
    
    if (document.addEventListener) {
      $el.addEventListener("click", callBack, false);
      $closeBtn.addEventListener("click", closePrompt, false);
    } else if (document.attachEvent) {
      $el.attachEvent("click", callBack);
      $closeBtn.attachEvent("click", closePrompt);
    } else {
      $el.onclick = callBack;
      $closeBtn.onclick = closePrompt;
    }
  }());