
/** Send Product Page Event */
utag.data.event_category = 'conversion' ;
utag.data.event_action = 'product_view' ;
utag.data.event_label = 'product_page';
utag.data.event_value = '';


/** Send Recipe Page Event */
utag.data.event_category = 'conversion' ;
utag.data.event_action = 'recipe_view' ;
utag.data.event_label = 'recipe_page';
utag.data.event_value = '';

/** Send Content Page Event */
utag.data.event_category = 'conversion' ;
utag.data.event_action = 'content_view' ;
utag.data.event_label = '';
utag.data.event_value = '';

/** Send Coporate Page Event */
utag.data.event_category = 'conversion' ;
utag.data.event_action = 'corporate_view' ;
utag.data.event_label = 'corporate_page';
utag.data.event_value = '';