
/** page information & architecture */
    utag.data.website_section = "";
    utag.data.page_category = "";
    utag.data.page_subcategory = "";
    utag.data.page_type = ""; // i.e. product_page; recipe_page; homepage; catalogue; etc.
    utag.data.page_name = "";
    utag.data.breadcrumbs = ""; // in Array split by commas. 

/** Product Information */
    utag.data.product_id = "";
    utag.data.product_name = "";
    utag.data.product_image = ""; // as full url
    utag.data.product_legal = "";
    utag.data.product_brand = "";
    utag.data.product_category = "";
    utag.data.product_subcategory = "";

/** Related Product Information */
    utag.data.relatedProduct_name = [];
    utag.data.relatedProduct_img = []; // as full url
    utag.data.relatedProduct_url = [];
    utag.data.relatedProduct_brand = [];
    utag.data.relatedProduct_category = [];

/** Related Recipe Information */
    utag.data.relatedRecipe_name = [];
    utag.data.relatedRecipe_img = []; // as full url
    utag.data.relatedRecipe_url = [];
    
/** Recipe Information */
    utag.data.recipe_name = "";
    utag.data.recipe_image = ""; // as full url
    utag.data.recipe_category = ""; 
    utag.data.recipe_details = {skills:'str',duration:'str or num',healthiness:'str',servings:'num'};


/** Recipe Information */
    utag.data.recipe_name = "";
    utag.data.recipe_image = ""; // as full url
    utag.data.recipe_category = ""; 
    utag.data.recipe_details = {skills:'str',duration:'str or num',healthiness:'str',servings:'num'};


/** Recipe Information */
    utag.data.content_name = "";
    utag.data.content_image = ""; // as full url
    utag.data.content_category = ""; 